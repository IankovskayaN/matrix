#include <iostream>
#include <ctime>
#include <cmath>
#include <stdlib.h>

using namespace std;

int** mult(int **mat, int **bmat, int n, int m,  int l, int p)
{

	int **cmat = new int*[n];
	for (int i = 0; i<n; ++i)
	{
		cmat[i] = new int[l];
		for (int j = 0; j<l; ++j) {
			cmat[i][j] = 0;

			for (int s = 0; s < m; ++s)
				cmat[i][j] += (mat[i][s] * bmat[s][j]);
		}
	}
	return cmat;


}


int** transponir(int** mat, int n, int m)
{
	int **dmat = new int*[m];
	for (int i = 0; i<m; ++i)
	{
		dmat[i] = new int[n];
		for (int j = 0; j<n; ++j)
			dmat[i][j] = mat[j][i];

	}
	return dmat;

}
void printm(int** mat, int n, int m)
{
	for (int i = 0; i<n; ++i)
	{

		for (int j = 0; j<m; ++j)
		{
			cout.width(4);
			cout << mat[i][j] << " ";
		}
		cout << endl;
	}
	cout << endl;

}

int main()
{
	srand(time(NULL));
	int n = 0, m = 0, p = 0, l = 0;
	setlocale(LC_ALL, "Russian");
	cout << "�������  ������ 1 ������� : " << endl;
	cout << endl;
	cin >> n >> m;
	cout << endl;


	int **mat = new int*[n];
	for (int i = 0; i<n; ++i)
	{
		mat[i] = new int[m];
		for (int j = 0; j<m; ++j)
			mat[i][j] = rand() % 9 + 1;

	}
	cout << "������� A: " << endl;
	cout << endl;
	printm(mat, n, m);

	int **dmat = transponir(mat, n, m);
	cout << "����������������� �: " << endl;
	cout << endl;
	printm(dmat, m, n);


	cout << "������� ������ 2 �������: " << endl;
	cout << endl;
	cin >> p >> l;
	cout << endl;
	int **bmat = new int*[p];
	for (int i = 0; i<p; ++i)
	{
		bmat[i] = new int[l];
		for (int j = 0; j<l; ++j)

			bmat[i][j] = rand() % 9 + 1;

	}

	cout << "B: " << endl;
	cout << endl;
	printm(bmat, p, l);

	if (p == m) //�������� �� ��������� ������
	{
		int **cmat = mult(mat, bmat, n, m, p, l);
		cout << "A*B: " << endl;
		cout << endl;
		printm(cmat, n, l);
		for (int i = 0; i<n; i++)
			delete[] cmat[i];
		delete[] cmat;
	}
	else
		cout << "Error/��������� ���������� " << endl;



	for (int i = 0; i<m; i++)
		delete[] dmat[i];
	delete[] dmat;

	for (int i = 0; i<n; i++)
		delete[] mat[i];
	delete[] mat;
	for (int i = 0; i<p; i++)
		delete[] bmat[i];
	delete[] bmat;
	system("pause");
	return 0;
}


